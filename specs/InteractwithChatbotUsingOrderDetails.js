//Test data
const TestData = require('../testdata/globalData.json')
const ShopDisneyHomepage = require('../FrameWork/pageActionsMethod/ShopDisneyHomePage.js')

/**
 *TestCase- Interacting with chatbot feature using Order Details
 */
describe("InteractingwithChatbotFeatureinShopDisney_Application", function() 
{

    it("TC001_UsingOrderDetails", function()
    {
        //Launch the ShopDisney application
        ShopDisneyHomepage.launchURL(TestData.URL.ShopDisney_url);

        //Verify the Shop Disney Home page
        ShopDisneyHomepage.verifyHomePage();

        //Click on Virtual Assistant icon{chatbot} in Home page
        ShopDisneyHomepage.clickOnVirtualAssistant();

        //Verify virtual assistant{chatbot} is opened or not
        ShopDisneyHomepage.verifyVirtualAssistant();

        /*
        Click on Order status option
        Expected Result - User should be able to see the following
        A) 'Glad to help! Do you have your order number and the email address associated with this order?' text is displayed or not.
        B) 'Yes', 'No' and 'Go Back' options are displayed or not.
        */
       ShopDisneyHomepage.clickOnOrderStatus();

       //Click on 'No' option & Verify the 'Locate Order Number' and 'No Email Address' options are displayed or not.
       ShopDisneyHomepage.clickOnNoOption();

       //Click on 'Locate Order Number' option & verify the preview with text 'How to find my Order Number' is displayed or not.
        ShopDisneyHomepage.clickOnLocateOrderNumber();

        /*
        Click on 'Close' icon in the preview with text 'How to find my Order Number'.
        Expected Result - User should be able to see the following
        A) 'Was it helpful' text is displayed or not.
        B) 'Yes, I found my order number' and 'No, I could not find order email' options are displayed or not.
        */
       ShopDisneyHomepage.clickOnCloseIconInHowtoFindMyOrderNumberPreview();

       /*
       Click on 'Yes, I found my order number' option.
       Expected Result - User should be able to see the following
       A) 'Great. Do you want me to try looking up your order details?' text is displayed or not.
       B) 'Yes' and 'No' options are displayed or not.
        */
       ShopDisneyHomepage.clickOnYesIFoundMyOrderNumber();

       //Click on 'Yes' option & Verify the text 'Please provide your Order Number' text is displayed or not.
       ShopDisneyHomepage.clickOnYesOption();

       //Enter Order Details
       ShopDisneyHomepage.enterOrderDetails(TestData.Data.OrderNumber, TestData.Data.EmailAddress);

       /*
       Purpose-Click on 'send message' icon
       Expected Result - User should be able to see the following
       A) 'I am not able to find an order for the Order Number and Email Address you provided.' text is displayed or not.
       B) 'Yes' and 'No' options are displayed or not.
       */
      ShopDisneyHomepage.clickOnSendMessageIcon();

      /*
      Click on 'No' Option
       Expected Result - User should be able to see the following
      A) 'No problem. Let me connect you with a cast member for further help.' text is displayed or not.
      B) 'Chat with Cast Member' option is displayed or not.
      */
      ShopDisneyHomepage.clickOnNoOptiontoCheckWithAnotherEmail();
    })

})