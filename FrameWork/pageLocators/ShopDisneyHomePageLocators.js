class ShopDisneyHomePageLocators
{
    constructor()
    {
    this.HEAD= '//pre[contains(text(),"API endpoint")]';
    this.SHOPDISNEY_LOGO= '//span[text()="Shop Disney"]/parent::a';
    this.VIRTUALASSISTANT= 'div[class="avatar"]>a';
    this.VIRTUALASSISTANT_IFRAME= '[id="insync-iframe"]';
    this.VIRTUALASSISTANT_HEADER= '.insyncChatHeaderText';
    this.ORDERSTATUS_OPTION= '//div[@class="insyncButtonContainer  insyncShow"]//button[text()="Order Status"]';
    this.SOMETHINGELSE_OPTION= '//div[@class="insyncButtonContainer  insyncShow"]//button[text()="Something Else"]';
    this.ORDERNUMBERANDEMAILASSOCIATED_TEXT= '//div[contains(text(),"your order number and the email address associated")]'
    this.YES_OPTION= '//div[@id="insyncChatbox"]/div[2]/div/div[3]/div/div[2]/div[1]/div/div[2]/div/div[1]'
    this.NO_OPTION= '//div[@id="insyncChatbox"]/div[2]/div/div[3]/div/div[2]/div[1]/div/div[2]/div/div[2]'
    this.GOBACK_OPTION= '//div[@id="insyncChatbox"]/div[2]/div/div[3]/div/div[2]/div[1]/div/div[2]/div/div[3]'
    this.LOCATEORDERNUMBER_OPTION= '//div[@class="insyncButtonContainer  insyncShow"]/div/button[text()="Locate Order"]'
    this.NOEMAIlADDRESS_OPTION= '//div[@class="insyncButtonContainer  insyncShow"]/div/button[text()="No Email Address"]'
    this.FINDMYORDERNUMBER_PREVIEW_IFRAME= '[id="inysnc-iframe"]'
    this.HOWTOFINDMYORDERNUMBER_PREVIEW= '[class="faq-body"]>h3'
    this.CLOSEICON_PREVIEW= '[class="inysnc-close"]'
    this.WASITHELPFUL_TEXT= '//div[text()="Was it helpful?"]'
    this.YESIFOUNDMYORDERNUMBER_OPTION= '//div[text()="Yes, I found my order number"]'
    this.NOICOUNDNOTFINDORDEREMAIL_OPTION= '//div[text()="No, I could not find order email"]'
    this.TRYLOOKINGUPYOURORDERDETAILS_TEXT= '//div[contains(text(),"try looking up your order details?")]'
    this.ORDERDETAILS_YES_OPTION= '//div[contains(text(),"want me to try looking up your order")]/ancestor::div/following-sibling::div/div/div[text()="Yes"]'
    this.ORDERDETAILS_NO_OPTION='//div[contains(text(),"want me to try looking up your order")]/ancestor::div/following-sibling::div/div/div[text()="No"]'
    this.PLEASEPROVIDEYOURORDERNUMBER_TEXT= '//div[text()="Please provide your "]//b[text()="Order Number"]'
    this.DESCRIPTION_TEXTFIELD= '[name="description"]'
    this.SENDMESSAGE_ICON= '[id="Arrow"]'
    this.EMAILADDRESSASSOCIATEDWITHTHISORDER_TEXT= '//div[text()="Thanks. And what is the "]//b[text()="Email Address"]'
    this.NOTABLETOFINDANORDERNUMBERANDEMAIL_TEXT= '//div[contains(text(),"not able to find an order")]//b[text()="Order Number"]/following-sibling::b[text()="Email Address"]'
    this.CHECKANOTHEREMAIL_YES_OPTION= '//div[contains(text(),"not able to find an order")]/ancestor::div/following-sibling::div/div/div[text()="Yes"]'
    this.CHECKANOTHEREMAIL_NO_OPTION= '//div[contains(text(),"not able to find an order")]/ancestor::div/following-sibling::div/div/div[text()="No"]'
    this.CONNECTWITHCASTMEMBER_TEXT= '//div[contains(text(),"connect you with a cast member")]'
    this.CHATWITHCASTMEMBER_OPTION= '//div[text()="Chat with Cast Member"]'


    }
}

module.exports = new ShopDisneyHomePageLocators();