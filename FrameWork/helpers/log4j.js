const log4js = require('log4js');
log4js.configure({
    appenders: { zenq: { type: 'file', filename: 'reports/logs/Log4j.log' }, console: { type: 'console' } },
    categories: { default: { appenders: ['zenq', 'console'], level: 'trace', }
    }
});
// Create the logger
//const logger = log4js.getLogger();

// Log some messages
// logger.trace('Trace, log4js!');
// logger.debug('Debug, log4js!');
// logger.info('Hello, log4js!');
// logger.warn('Heads up, log4js!');
// logger.error('Danger, log4js!');
// logger.fatal('Fatal, log4js!');

/*const logger = log4js.getLogger('cheese');
logger.trace('Entering cheese testing');
logger.debug('Got cheese.');
logger.info('Cheese is Gouda.');
logger.warn('Cheese is quite smelly.');
logger.error('Cheese is too ripe!');
logger.fatal('Cheese was breeding ground for listeria.');*/