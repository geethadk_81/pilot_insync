const allureReporter = require('@wdio/allure-reporter')
const wdio = require('webdriverio');
//var assert = require('chai').assert;
const log4js = require('log4js');
const logger = log4js.getLogger('zenq');
class Utilites {
     /**
         * This method will assert element enabled with false
         * @param {*} element  - browser element
         * @param {*} elementDesc - browser element description 
         */
      async assertFalseEnabled(elementLocator, elementDesc) {
        const element = await browser.$(elementLocator);
        await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');
        expect(element.isEnabled()).toBeFalsy(elementDesc + ' is enabled');
    }
    /**
    * This method will assert element displayed with false
    * @param {*} element  - browser element
    * @param {*} elementDesc - browser element description 
    */
    async assertFalseDisplayed(elementLocator, elementDesc) {
        const element = await browser.$(elementLocator);
        await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');
        expect(element.isDisplayed()).toBeFalsy(elementDesc + ' is Displayed');
    }
    /**
    * This method will assert element displayed with true
    * @param {*} element  - browser element
    * @param {*} elementDesc - browser element description 
    */
    async assertTrueDisplayed(elementLocator, elementDesc) {
        const element = await browser.$(elementLocator);
        await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');
        expect(element.isDisplayed()).toBeTruthy(elementDesc + ' is not displayed');
    }
    /**
    * This method will assert element enabled with true
    * @param {*} element  - browser element
    * @param {*} elementDesc - browser element description 
    */
    async assertTrueEnabled(elementLocator, elementDesc) {
        const element = await browser.$(elementLocator);
        await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');
        expect(element.isEnabled()).toBeTruthy(elementDesc + ' is not enabled');
    }
    /**
    * This element will wait and verify whether the browser element exists on the screen
    * @param {*} element  - browser element
    * @param {*} elementDesc - browser element description 
    */
    async waitAndVerifyElement(elementLocator, elementDesc) {
        try {
            const element = await browser.$(elementLocator);
            await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');
            await this.browserVerifyElementDisplayedAsync(element, elementDesc);
        } catch (error) {

        }

    }

    /**
    * This element will wait for the browser element exists on the screen
    * @param {*} element  - browser element
    * @param {*} elementDesc - browser element description 
    */
    async waitForElement(elementLocator, elementDesc) {
        const element = await browser.$(elementLocator);
        await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');

    }
    /**
     * This element will verify whether the browser element exists on the screen
     * @param {*} element  - browser element
     * @param {*} elementDesc - browser element description 
     */
    async verifyElement(elementLocator, elementDesc) {
        const element = await browser.$(elementLocator);
        await this.browserVerifyElementDisplayedAsync(element, elementDesc);
    }
    /**
    * This element will verify whether the browser element exists on the screen
    * @param {*} element  - browser element
    * @param {*} elementDesc - browser element description 
    */
    async verifyElementNotDisplayed(elementLocator, elementDesc) {
        const element = await browser.$(elementLocator);
        this.browserVerifyElementNotDisplayedAsync(element, elementDesc);
    }
    /**
     * This method will assert element text with expected text parameter
     * @param {*} element  - browser element
     * @param {*} elementDesc - browser element description 
     * @param {*} expectedText 
     */
    async assertText(elementLocator, elementDesc, expectedText) {
        const element = await browser.$(elementLocator);
        await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');
        expect(expectedText).toBe(await element.getText(), element + ' text is not matching. Expected:' + expectedText + ' actual: ' + await element.getText());
    }
    /**
    * This method will wait until the browser element is visible and performs click action on it.
    * @param {*} element  - browser element
    * @param {*} elementDesc - browser element description 
    */
    async clickOnElement(elementLocator, elementDesc) {
        try {
            const element = await browser.$(elementLocator);
            await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');
            this.browserVerifyElementDisplayedAsyncWOS(element, elementDesc);
            await element.click();
            this.browserlogTextAsyncWOS(`${elementDesc} is clicked`, 'passed');
        } catch (error) {

        }

    }
    /**
     * This element will wait and set the text for the browser element with text which is passed as input
     * @param {*} element  - browser element
     * @param {*} elementDesc - browser element description 
     * @param {*} text - input text to set the text for the browser element
     */
    async setElementText(elementLocator, elementDesc, message) {
        try {
            const element = await browser.$(elementLocator);
            await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');
            await element.setValue(message);
            await this.browserlogTextAsync(elementDesc + " filed is set with text " + message, 'passed');
        } catch (error) {

        }

    }

    /**
     * This method will return the browser element text
     * @param {*} element  - browser element
     * @param {*} elementDesc - browser element description 
     * @returns - browser element text
     */
    async getElementText(elementLocator, elementDesc) {
        const element = await browser.$(elementLocator);
        await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');
        const elementText = await element.getText();
        await this.browserlogTextAsync(elementDesc + ' text on the page is ' + elementText, 'passed');
        return elementText;
    }
    /**
     * This method will return the browser element text by property value
     * @param {*} element  - browser element
     * @param {*} elementDesc - browser element description 
     * @returns - browser element text
     */
    async getElementTextByValue(elementLocator, elementDesc) {
        const element = await browser.$(elementLocator);
        await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');
        const elementText = await element.getProperty('value');
        await this.browserlogTextAsync(elementDesc + ' text on the page is ' + elementText, 'passed');
        return elementText;
    }
    /**
    * This method will select the browser element by attribute with value
    * @param {*} element  - browser element
    * @param {*} elementDesc - browser element description 
    * @param {*} attribute  - browser element attribute
    * @param {*} value  - browser element value
    * @returns - browser element text
    */
    async selectElementByAttribute(elementLocator, elementDesc, attribute, value) {
        const element = await browser.$(elementLocator);
        await this.waitForElementDisplayedAsync(element, elementDesc, elementDesc + ' is not displayed');
        const elementText = await element.selectByAttribute(attribute, value);
        await this.browserlogTextAsync(elementDesc + ' selected by attribute ' + attribute + " with value" + value, 'passed');
        return elementText;
    }
    /**
     * 
     * @param {*} element  - browser element
     * @param {*} elementDesc - browser element description 
     * @returns - true/false based on the displayed of browser element on web page
     */
    async isElementDisplayed(elementLocator, elementDesc) {
        const element = await browser.$(elementLocator);
        return element.isDisplayed();
    }
    /**
         * 
         * @param {*} element  - browser element
         * @param {*} elementDesc - browser element description 
         * @returns - true/false based on the whether the browser element on web page is enabled/Disabled respectively
         */
    async isElementEnabled(elementLocator, elementDesc) {
        const element = await browser.$(elementLocator);
        return element.isEnabled();
    }

    /**
     * Save element screenshot on web browser object
     * @param {*} elementLocator - element locator
     * @param {*} path - file path
     */
    async saveElementScreenshot(elementLocator, path) {
        const element = await browser.$(elementLocator);
        await element.saveScreenshot(path);
    }


    /**
    * @desc Async method to print a custom log on passed or failed and includes screenshot attachment
    * @param message message to write in log
    * @param result can be only failed or passed
    */
    async browserlogTextAsync(message, result) {
        try {
            allureReporter.addStep(`'${message}'`, {}, result);
            allureReporter.addAttachment(`'${message}'`,  Buffer.from(await browser.takeScreenshot(), 'base64'));
            //await allureReporter.addStep('screenshot', await Buffer.from(await browser.takeScreenshot(), 'base64'));
        } catch (error) {
            console.log("Error in caputing the screenshot. Details"+error);
        }
       
    }
    /**
       * @desc Async method to print a custom log on passed or failed and excludes screenshot attachment
       * @param message message to write in log
       * @param result can be only failed or passed
       */
    async browserlogTextAsyncWOS(message, result) {
        allureReporter.addStep(`'${message}'`, {}, result);
        //await allureReporter.addAttachment(`'${message}'`, await Buffer.from(await browser.takeScreenshot(), 'base64'));
        //await allureReporter.addStep('screenshot', await Buffer.from(await browser.takeScreenshot(), 'base64'));
    }
    /**
     * @desc Async method to verify element is displayed and to enter the details in the report logs
     * @param element
     * @param elementDesc
     * @returns {Promise<void>}
     */
    async browserVerifyElementDisplayedAsync(element, elementDesc) {
        try {
            const res = await element.isDisplayed();
            if (res) {
                expect(res).toBe(true);
                await this.browserlogTextAsync(`${elementDesc} is Displayed`, 'passed');
            } else {
                await this.browserlogTextAsync(`${elementDesc} is Not Displayed`, 'failed');
            }
        } catch (error) {
            console.log("Error @browserVerifyElementDisplayedAsync method. Details:"+error);
        }
       
    }
    /**
    * @desc Async method to verify element is not displayed and to enter the details in the report logs
    * @param element
    * @param elementDesc
    * @returns {Promise<void>}
    */
    async browserVerifyElementNotDisplayedAsync(element, elementDesc) {
        const res = await element.isDisplayed();
        if (res) {
            expect(res).toBe(false);
            await this.browserlogTextAsync(`${elementDesc} is Displayed`, 'failed');
        } else {
            await this.browserlogTextAsync(`${elementDesc} is not Displayed`, 'passed');
        }
    }
    /**
        * @desc Async method to verify element is displayed and to enter the details in the report logs without screenshot
        * @param element
        * @param elementDesc
        * @returns {Promise<void>}
        */
    async browserVerifyElementDisplayedAsyncWOS(element, elementDesc) {
        const res = await element.isDisplayed();
        if (res) {
            expect(res).toBe(true);
            await this.browserlogTextAsyncWOS(`${elementDesc} is Displayed`, 'passed');
        } else {
            await this.browserlogTextAsyncWOS(`${elementDesc} is Not Displayed`, 'failed');
        }
    }

    /**
       * @desc waits for the specified element
       * @param element Web element
       * @param name WebElement name/description
       * @param errorMsg custom message on test failure
       * @returns {boolean | Promise<boolean>}
       */
    waitForElementDisplayed(element, name, errorMsg) {
        return element.waitForDisplayed({
            timeout: 30000,
            timeoutMsg: `${errorMsg}`,
            reverse: false,
        });
    }
    /**
    * @desc waits for the specified element
    * @param element Web element
    * @param errorMsg custom message on test failure
    * @returns {boolean | Promise<boolean>}
    */
    async waitForElementDisplayedAsync(element, errorMsg) {
        return element.waitForDisplayed({
            timeout: 90000,
            timeoutMsg: `${errorMsg}`,
            reverse: false,
        });
    }
    /**
        * @desc method to verify if element is displayed
        * @param element
        * @param elementDesc
        */
    verifyElementDisplayed(element, elementDesc) {
        driver.pause(1000);
        const res = element.isDisplayed();
        if (res) {
            expect(res).toBe(true);
            this.logText(`${elementDesc} is Displayed`, 'passed');
        } else {
            expect(res).toBe(true);
            this.logText(`${elementDesc} is Not Displayed`, 'failed');
        }
    }

    /**
     * @desc Async method to verify element is displayed
     * @param element
     * @param elementDesc
     * @returns {Promise<void>}
     */
    async verifyElementDisplayedAsync(element, elementDesc) {
        const res = await element.isDisplayed();
        if (res) {
            expect(res).toBe(true);
            await this.logTextAsync(`${elementDesc} is Displayed`, 'passed');
        } else {
            await this.logTextAsync(`${elementDesc} is Not Displayed`, 'failed');
        }
    }
    /**
        * @desc method to print a custom log on passed or failed
        * @param message message to write in log
        * @param result can be only failed or passed
        */
    logText(message, result) {
        allureReporter.addStep(`'${message}'`, {}, result);
        allureReporter.addStep('screenshot', Buffer.from(browser.takeScreenshot(), 'base64'));
    }

    /**
     * @desc Async method to print a custom log on passed or failed
     * @param message message to write in log
     * @param result can be only failed or passed
     */
    async logTextAsync(message, result) {
        allureReporter.addStep(`'${message}'`, {}, result);
        allureReporter.addStep('screenshot',  Buffer.from(await browser.takeScreenshot(), 'base64'));
    }
    /**
     * Gets an object containing the current page title
     */
    title() {
        try {
            var title;
            title = browser.getTitle();
            logger.info("Page title is " + title);
            return title;
        } catch (e) {
            if (e.name !== null) {
                logger.error("Failed to fetch page title - " + e);
                assert.fail("Failed to fetch page title \n" + e);
            }
        }
    }
     /**
     * Waits
     */
      waitForVisible(locator_, time_, friendlyNameOfElement, shouldFail='true') {
        try {
            const elem = $(locator_);
            let options= {timeout:3000};
            if (typeof time_=="undefined"){
                logger.info("Using default timeout of 3000ms");
            }
            else {
                options.timeout=time_;
            }
            const waitForDisplayed = elem.waitForDisplayed(options);
            if (time_ !== "") {
                logger.info("Waited " + time_ + "ms for " + friendlyNameOfElement + " to be displayed");
            }
            return waitForDisplayed;
        } catch (e) {
            if (e.name !== null) {
                if (shouldFail) {
                    logger.error(friendlyNameOfElement + " is not displayed - " + e);
                    assert.fail("'" + friendlyNameOfElement + "' is not displayed \n" + e);
                }
            }
        }
    }
    /**
     * @param locator_
     * @returns {WebDriver}
     * Scrolls to given locator by taking x and y coordinates
     */
    scrollTo(locator_, friendlyNameOfElement) {
        try {
            $(locator_).scrollIntoView();
            logger.info("scrolled " + friendlyNameOfElement + " into view");
            return this;
        } catch (e) {
            if (e.name !== null) {
                logger.error("Failed to scroll " + friendlyNameOfElement + " into view - " + e);
                assert.fail("Failed to scroll " + friendlyNameOfElement + " into view \n" + e);
            }
        }
    }
    //get the Text from the given locator
    safeGetText(locator_, time_, friendlyNameOfElement) {
        try {
            this.waitForVisible(locator_, time_, friendlyNameOfElement);
            let text;
            this.scrollTo(locator_, friendlyNameOfElement);
            text = $(locator_).getText();
            logger.info("Fetching text- " + text + " for Element " + friendlyNameOfElement);
            if (!text) {
                text = "";
            }
            logger.info("Fetched text with scroll of - " + friendlyNameOfElement + " as - " + text);
            return text;
        } catch (e) {
            if (e.name !== null) {
                logger.error("Failed to fetch text with scroll of - " + friendlyNameOfElement + " in " + time_ + "ms");
                assert.fail("Failed to fetch text with scroll of - " + friendlyNameOfElement + " in " + time_ + "ms");
            }
        }
    }
    /**
     * Purpose - To wait until the element is visible and return true / false
     * @param locator_
     * @param time_ in milli seconds
     * @param friendlyNameOfElement
     * @returns {boolean}
     */
    isVisibleWait(locator_, time_, friendlyNameOfElement) {
        try {
            let isVisible = false;
            const elem = $(locator_);
            let i = 0;
            logger.info("Waiting until element '" + friendlyNameOfElement + "' is visible")
            while (i <= time_) {
                isVisible = elem.isDisplayed();
                if (isVisible) {
                    logger.info("'" + friendlyNameOfElement + "' is displayed after " + i);
                    break;
                } else {
                    browser.pause(500);
                    logger.error(friendlyNameOfElement + " is not displayed after " + i);
                    i = i + 500;
                }
            }
            return isVisible;
        } catch (e) {
            if (e.name !== null) {
                logger.error(friendlyNameOfElement + " is not displayed - " + e);
                assert.fail("'" + friendlyNameOfElement + "' is not displayed \n" + e);
            }
        }
    }
    safeAsserts(verification, actual, friendlyText, expected) {
        switch (verification) {

            case 'equal':
                expect(expected, friendlyText).to.equal(actual);
                break;
            case 'contains':
                expect(actual, friendlyText).to.contains(expected);
                break;
            case 'true':
                expect(actual, friendlyText).to.be.true;
                break;
            case 'false':
                expect(actual, friendlyText).to.be.false;
                break;
            case 'deepEqual':
                expect(actual, friendlyText).to.deep.equal(expected);
                break;
        }
    }
     /**
     * Selects an option from a combo box
     */
      selectByText(locator_, text_, friendlyNameOfElement) {
        this.domStatus();
        try {
            if (text_) {
                $(locator_).selectByVisibleText(text_);
                logger.info("selected " + text_ + " by text for " + friendlyNameOfElement);
            }
            return this;
        } catch (e) {
            if (e.name !== null) {
                logger.error(friendlyNameOfElement + " was not found - " + e);
                assert.fail("Failed to select value for '" + friendlyNameOfElement + "'\n" + e);
            }
        }
    }
    domStatus() {
        browser.waitUntil(
            function () {
                const state = browser.execute(function () {
                    return document.readyState;
                });
                return state === 'complete';
            },{timeout:180000,timeoutMsg:'Page is not completely loaded in 180 seconds'}
        );
    }
    safeVisibleClick(locator_, time_, friendlyNameOfElement) {
        try {
            const ele = $(locator_);
            this.waitForVisible(locator_, time_, friendlyNameOfElement);
            ele.click();
            logger.info("Clicked on " + friendlyNameOfElement);
            return this;
        } catch (e) {
            if (e.name !== null) {
                logger.error(friendlyNameOfElement + " was not found - " + e);
                assert.fail("Failed to click '" + friendlyNameOfElement + "'\n" + e);
            }
        }
    }
    //Updates the locator with given string value and return locator
    dynamicLocators(locator_, textToBeUpdated) {
        let updatedLocator = locator_.replace("%s", textToBeUpdated);
        logger.info("The Dynamic Locator is: " + updatedLocator);
        return updatedLocator;
    }
    //Sets an element to a given value
    safeSetValue(locator_, text_, time_, friendlyNameOfElement) {
        try {
            const ele = $(locator_);
            this.waitForVisible(locator_, time_, friendlyNameOfElement);
            ele.setValue(text_);
            logger.info("Entered text in " + friendlyNameOfElement + " as " + text_);
            return this;
        } catch (e) {
            if (e.name !== null) {
                logger.error(friendlyNameOfElement + " was not found - " + e);
                assert.fail("Failed to enter text in '" + friendlyNameOfElement + "' \n " + e);
            }
        }
    }
    waitUntilDomGetsLoaded() {
        let n = 10;
        let bool = true;
        for (let i = 0; i < n; i++) {
            if (bool) {
                browser.waitUntil(
                    function () {
                        const state = browser.execute(function () {
                            return document.readyState;
                        });
                        if (state === "complete") {
                            bool = false
                        }
                        return state === 'complete';
                    },{timeout:10000,timeoutMsg:'Oopse! Time out!'}
                );
            }
        }
    }
    //Navigates to given URL
    navigate(destUrl_) {
        try {
            if (browser.isMobile == false) {
                browser.maximizeWindow();
            }
            browser.url(destUrl_);
            logger.info("Navigated to " + destUrl_);
            return this;
        } catch (e) {
            if (e.name !== null) {
                logger.error(destUrl_ + " is not valid - " + e);
                assert.fail("Failed to navigate to '" + destUrl_ + "'\n" + e);
            }
        }
    }
    //Updates the locator with given string value and return locator
    multipleDynamicLocators(locator_, textToBeUpdated) {
        let updatedLocator = locator_.replace("%n", textToBeUpdated);
        logger.info("The Dynamic Locator is: " + updatedLocator);
        return updatedLocator;
    }
    /**
     * Maximizes given window if not already maximized
     *  Using "current" or a falsy value will maximize the currently active window
     */
     windowHandleMaximize() {
        try {
            browser.maximizeWindow();
            logger.info("Maximized browser window");
            return this;
        } catch (e) {
            if (e.name !== null) {
                logger.error("Failed to maximize window - " + e);
                assert.fail("Failed to maximize window \n" + e);
            }
        }
    }
    mouseOver(locator_, friendlyNameOfElement) {
        this.domStatus();
        try {
            const ele = $(locator_);
            this.waitForVisible(locator_, '', friendlyNameOfElement);
            ele.moveTo();
            logger.info("Mouse over to " + friendlyNameOfElement);
        } catch (e) {
            if (e.name !== null) {
                logger.error("Failed to mouse Over to " + friendlyNameOfElement + " - " + e);
                assert.fail("Failed to mouse Over to " + friendlyNameOfElement + "\n" + e);
            }
        }
    }
    //Clicks on the element using Java Script executor
    safeJavaScriptClick(locator_, time_, friendlyNameOfElement) {
        try {
            const ele = $(locator_);
            this.waitForVisible(locator_, time_, friendlyNameOfElement);
            browser.execute("arguments[0].click();", ele);
            logger.info("Clicked on the element " + friendlyNameOfElement);
        } catch (e) {
            if (e.name !== null) {
                logger.error(friendlyNameOfElement + " was not found - " + e);
                assert.fail("Failed to click '" + friendlyNameOfElement + "'\n" + e);
            }
        }
    }
    browserkeys(value_, friendlyNameOfKey) {
        try {
            browser.keys(value_);
            logger.info(friendlyNameOfKey + " key has been clicked");
            return this;
        } catch (e) {
            if (e.name !== null) {
                logger.error("Failed to click " + friendlyNameOfKey + " key - " + e);
                assert.fail("Failed to click " + friendlyNameOfKey + " key  \n" + e);
            }
        }
    }
    /**
     * Gets value of given locator
     */
    getValue(locator_, friendlyNameOfElement) {
        try {
            var text;
            text = $(locator_).getValue();
            logger.info("Fetched value of " + friendlyNameOfElement + " as - " + text);
            if (!text) {
                text = "";
            }
            return text;
        } catch (e) {
            if (e.name !== null) {
                logger.error("Failed to fetch value of " + friendlyNameOfElement + " - " + e);
                assert.fail("Failed to fetch value of " + friendlyNameOfElement + "\n" + e);
            }
        }
    }

    clearElement(locator_, friendlyNameOfElement) {
        try {
            $(locator_).clearValue();
            logger.info("Cleared text value of " + friendlyNameOfElement);
            return this;
        } catch (e) {
            if (e.name !== null) {
                logger.error(friendlyNameOfElement + " was not found - " + e);
                assert.fail("Failed to clear text value of '" + friendlyNameOfElement + "'\n" + e);
            }
        }
    }

    //Select the value from dropdown using java Script via Visible text
   safeSelectByJS(locator_, visibleTextOptionToSelect, friendlyNameOfElement)
    {
    const ele = $(locator_);
    logger.info("the value to be selected through Java Script Select- " + friendlyNameOfElement + " from dropDown is " + visibleTextOptionToSelect);
    browser.execute(
        function () {
            let select = arguments[0];
            for (let i = 0; i < select.options.length; i++) {
                if (select.options[i].text == arguments[1]) {
                    select.options[i].selected = true;
                }
            }
        }, ele, visibleTextOptionToSelect);
    logger.info("the value selected through Java Script Select- " + friendlyNameOfElement + " from dropDown is " + visibleTextOptionToSelect);
    }

    //Checks if given value is null
    checkIfNull(value_, baseFunc_) {
        if (value_ != null) {
            baseFunc_();
        }
    }
    //Pause for given length
    safePause(length_,friendlyReasonForWait) {
        this.checkIfNull(length_, function () {
            browser.pause(length_);
            logger.info("Static wait for "+length_+"ms "+friendlyReasonForWait);
        });
        return this;
    }
    /**
    * @param {} elementLocator - frame/iframe locator
    * @param {} elementDesc
    */
    async switchToFrame(elementLocator,elementDesc){
        try{
        const element = await browser.$(elementLocator);
        await this.waitForElementDisplayedAsync(element, elementDesc + ' is not displayed');
        await browser.switchToFrame(element);
        logger.info("Switched to Frame "+elementDesc)
        }
        catch(error){
            logger.error(elementDesc + " was not found - " + error);
            assert.fail("No Frame present '" + friendlyNameOfElement + "'\n" + error);

        }
    }

    /**
    * @param {} elementDesc
    */
    async switchToParentFrame(elementDesc){
        try{
            await browser.switchToParentFrame();
            logger.info("Switched to Parent Frame "+elementDesc)
        }
        catch(error){
            logger.error(elementDesc + " was not found - " + error);
            assert.fail("No Frame present'" + friendlyNameOfElement + "'\n" + error);
        }
    }
    /**
    * @param {} url
    */
   launchURL(url){
    browser.url(url)
    browser.maximizeWindow()
    logger.info("Launching the url - "+url)
    this.waitUntilDomGetsLoaded();
   }

};

//export default new Utilites();
module.exports = new Utilites();