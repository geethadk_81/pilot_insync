# WDIO Framework

## Installing nodejs
Click here to get nodejs(V 14.x.x) and install using below links.
    For [WindowOS](https://nodejs.org/dist/v14.15.1/node-v14.15.1-x86.msi)
    For [MacOs](https://nodejs.org/dist/v14.16.1/node-v14.16.1.pkg)

## Clone the git repository
Post NodeJS installation, clone the WDIO framework repository using below command
```bash
    cd <Path_To_Folder_To_Cloned_Copy>
    git clone "<GitRepoUrl>" "<folderNameForLocalClone>"
```

## Install node modules
Open the local clone with IDE [Visual studio code - open source] to install node modules using below command
```bash
    npm install
```
## Folder Structure
WDIO Framwork has below listed folder.\
-> allure-report - The folder contains all execution level allure logs which will be used while fetching the execution reports.\
-> node_modules - The folder contains node modules. This will only be created after running the "npm install" command.\
-> specs - Contians test scripts module wise
-> testdata - Contains all the application related test data which may include URLs, credentials, inputs and so on.\
-> Framework - Framework folder contains sub-folders and are listed below.\
    - helpers - Contains common methods which can be utilized while writing the scripts to reduce the line of code.\
    - pageLocators - Locators related to the application in test are placed page wise.\
    - pageActionsMethod - Contians the actions interacting with the application.\
-> testRunners - Contains batch file which triggers the execution with respect to the config file {wdio.conf.js}.

## Commands
Install the node modules
```bash
    npm install
```
## Execution
Run the batch file to trigger the tests which are part of the specs under the browser config.\
**Path to the batch file [testRunners/chrome]**



